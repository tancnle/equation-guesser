# frozen_string_literal: true

require 'minitest/autorun'
require './lib/equation_guesser'

describe EquationGuesser do
  before do
    @operands = [1, 2, 3]
    @desired_result = 1
    @renderer = StringIO.new
    @guesser = EquationGuesser.new(@operands, @desired_result, @renderer)
  end

  describe '#guess' do
    before do
      @number_of_guesses = 3
      @guesser.guess(@number_of_guesses)
      @renderer.rewind

      @output = @renderer.readlines.map(&:strip)
      @measurement_info = @output.pop
    end

    it 'displays list of equations and best guess to renderer' do
      assert_equal(
        [
          '(1 + (2 + 3)) = 6 (5)',
          '(1 - (2 + 3)) = -4 (-5)',
          '(1 * (2 + 3)) = 5 (4)',
          'Guesses 3',
          'Best Guess: (1 * (2 + 3)) = 5 (4)'
        ],
        @output
      )
    end

    it 'displays measurement info' do
      assert_match('Execution time', @measurement_info)
    end
  end

  describe '#guess_algorithm' do
    describe 'number of guesses is lower than possible solutions' do
      before do
        @number_of_guesses = 3
        @result = @guesser.send(
          :guess_algorithm,
          @operands,
          @desired_result,
          @number_of_guesses
        )
      end

      it 'returns number of solutions corresponding to number of guesses' do
        assert_equal(@number_of_guesses, @result.send(:solutions).size)
      end
    end

    describe 'number of guesses is higher than possible equation permutation' do
      before do
        @operands = [1, 2, 3]
        @desired_result = 4.5 # this is impossible to find :)
        @number_of_guesses = 100
        @result = @guesser.send(
          :guess_algorithm,
          @operands,
          @desired_result,
          @number_of_guesses
        )
      end

      it 'displays all possible solutions' do
        assert_equal(93, @result.send(:solutions).size)
      end
    end

    describe 'found exact match before number of guesses exhausted' do
      before do
        @operands = [1, 2, 3]
        @desired_result = -4
        @number_of_guesses = 3
        @result = @guesser.send(
          :guess_algorithm,
          @operands,
          @desired_result,
          @number_of_guesses
        )
      end

      it 'terminates search and displays results' do
        # '(1 + (2 + 3)) = 6 (10)'
        # '(1 - (2 + 3)) = -4 (0)'   <- matched
        # '(1 * (2 + 3)) = 5 (9)'

        assert_equal(2, @result.send(:solutions).size)
      end
    end
  end
end
