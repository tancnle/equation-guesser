# frozen_string_literal: true

require 'minitest/autorun'
require './lib/solver'

describe Solver do
  before do
    @solver = Solver.new
  end

  describe '#execute' do
    before do
      equation = Minitest::Mock.new
      equation.expect(:result, 20)
      desired_result = 24

      @solution = @solver.solve(
        equation: equation,
        desired_result: desired_result
      )
    end

    it 'returns difference between equation result and desired result' do
      assert_equal(-4, @solution.difference)
    end
  end
end
