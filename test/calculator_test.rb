# frozen_string_literal: true

require 'minitest/autorun'
require './lib/calculator'
require './lib/expression'

describe Calculator do
  describe '#calculate' do
    before do
      @expression = Expression.new([6, 3, 12, 8, 4, '+', '/', '*', '-'])
      @calculator = Calculator.new
    end

    it 'returns an equation with result' do
      equation = @calculator.calculate(@expression)

      assert_equal(
        @expression,
        equation.expression
      )

      assert_equal(
        3,
        equation.result
      )
    end

    describe 'divided by 0' do
      before do
        @expression = Expression.new([12, 8, 8, '-', '/'])
        @calculator = Calculator.new
      end

      it 'returns an equation without result' do
        equation = @calculator.calculate(@expression)

        assert_equal(
          @expression,
          equation.expression
        )

        assert_nil equation.result
      end
    end
  end
end
