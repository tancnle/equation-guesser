# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/benchmark'
require './lib/equation_guesser'

describe 'Equation Guesser Benchmark' do
  bench_performance_linear 'baseline algorithm bench', 0.99 do |n|
    n.times do
      @operands = [1, 2, 3, 4]
      @desired_result = 10

      @equation_guesser = EquationGuesser.new(@operands, @desired_result, StringIO.new)
      @equation_guesser.guess(n)
    end
  end
end
