# frozen_string_literal: true

require 'minitest/autorun'
require './lib/report'

describe Report do
  before do
    solutions = ['1 + 2 = 3 (20)', '2 / 1 = 2 (21)']
    best_guess = '1 + 2 = 3 (20)'
    result = Minitest::Mock.new
    result.expect(:solutions, solutions)
    result.expect(:best_guess, best_guess)

    @report = Report.new(result)
  end

  describe '#to_s' do
    it 'displays lists of all guesses with best guess' do
      assert_equal(
        [
          '1 + 2 = 3 (20)',
          '2 / 1 = 2 (21)',
          'Guesses 2',
          'Best Guess: 1 + 2 = 3 (20)'
        ].join("\n"),
        @report.to_s
      )
    end
  end
end
