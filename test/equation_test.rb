# frozen_string_literal: true

require 'minitest/autorun'
require './lib/equation'

describe Equation do
  before do
    tokens = %w[6 3 12 8 4 + / * -]
    expression = Expression.new(tokens)
    result = 3

    @equation = Equation.new(expression: expression, result: result)
  end

  describe '#to_s' do
    it 'returns algebra infix expression with result' do
      assert_equal(
        '(6 - (3 * (12 / (8 + 4)))) = 3',
        @equation.to_s
      )
    end
  end
end
