# frozen_string_literal: true

require 'minitest/autorun'
require './lib/expression'

describe Expression do
  before do
    @tokens = %w[6 3 2 8 4 + / * -]
    @expression = Expression.new(@tokens)
  end

  describe '#to_s' do
    it 'returns algebra infix string representation of infix tokens' do
      assert_equal(
        '(6 - (3 * (2 / (8 + 4))))',
        @expression.to_s
      )
    end
  end

  describe '#infix' do
    it 'returns algebra infix tokens of RPN tokens' do
      assert_equal(
        ['6', '-', ['3', '*', ['2', '/', ['8', '+', '4']]]],
        @expression.send(:infix)
      )
    end
  end
end
