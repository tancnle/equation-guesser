# frozen_string_literal: true

require_relative 'solution'

class Solver
  def solve(equation:, desired_result:)
    difference = equation.result - desired_result
    Solution.new(equation: equation, difference: difference)
  end
end
