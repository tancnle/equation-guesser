# frozen_string_literal: true

class Equation
  attr_reader :expression, :result

  def initialize(expression:, result: nil)
    @expression = expression
    @result = result
  end

  def to_s
    [expression.to_s, '=', result].join(' ')
  end
end
