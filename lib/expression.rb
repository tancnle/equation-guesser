# frozen_string_literal: true

class Expression
  OPERATORS = %w[+ - * /].freeze

  include Enumerable

  def initialize(tokens)
    @tokens = tokens
  end

  def each(&block)
    @tokens.each(&block)
  end

  def to_s
    stringify(infix)
  end

  private

  def stringify(tokens)
    return tokens unless tokens.is_a? Array

    '(' + tokens.map { |t| stringify(t) }.join(' ') + ')'
  end

  def infix
    each_with_object([]) do |token, result|
      if OPERATORS.include? token
        second = result.pop
        first = result.pop
        result << [first, token, second]
      else
        result << token
      end
    end.first
  end
end
