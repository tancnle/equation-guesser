# frozen_string_literal: true

class Result
  def initialize(solutions:, best_guess:)
    @solutions = solutions
    @best_guess = best_guess
  end

  attr_reader :solutions, :best_guess
end
