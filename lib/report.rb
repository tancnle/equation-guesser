# frozen_string_literal: true

class Report
  def initialize(result)
    @result = result
  end

  def to_s
    guesses = @result.solutions

    [
      guesses,
      "Guesses #{guesses.size}",
      "Best Guess: #{@result.best_guess}"
    ].join("\n")
  end
end
