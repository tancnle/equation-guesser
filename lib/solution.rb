# frozen_string_literal: true

class Solution
  attr_reader :difference

  def initialize(equation:, difference:)
    @equation = equation
    @difference = difference
  end

  def to_s
    "#{@equation} (#{difference})"
  end
end
