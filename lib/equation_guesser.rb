# frozen_string_literal: true

require_relative 'calculator'
require_relative 'enumerator'
require_relative 'expression'
require_relative 'report'
require_relative 'result'
require_relative 'solver'

class EquationGuesser
  OPERATORS = %w[+ - * /].freeze

  def initialize(operands, desired_result, renderer = $stdout)
    @operands = operands
    @desired_result = desired_result
    @renderer = renderer
  end

  def guess(number_of_guesses)
    with_measurement(@renderer) do
      result = guess_algorithm(@operands, @desired_result, number_of_guesses)

      @renderer.puts report(result)
    end
  end

  private

  def guess_algorithm(operands, desired_result, number_of_guesses)
    solutions = expressions_generator(operands) \
                .map        { |expression| evaluate(expression) }
                .filter_map { |equation|   solve(equation, desired_result) if valid?(equation) }
                .take_until { |solution|   exact_match(solution) }
                .first(number_of_guesses)

    Result.new(
      solutions: solutions,
      best_guess: find_best_guess(solutions)
    )
  end

  def exact_match(solution)
    solution.difference.zero?
  end

  def find_best_guess(results)
    results.min { |a, b| a.difference.abs <=> b.difference.abs }
  end

  def report(result)
    Report.new(result)
  end

  def evaluate(expression)
    Calculator.new.calculate(expression)
  end

  def valid?(equation)
    !equation.result.nil?
  end

  def solve(equation, desired_result)
    Solver.new.solve(equation: equation, desired_result: desired_result)
  end

  def expressions_generator(operands)
    operands_permutation(operands).lazy.flat_map do |operand_permutation|
      operators_permutation(operands).map do |operator_permutation|
        Expression.new(operand_permutation + operator_permutation)
      end
    end
  end

  def operands_permutation(operands)
    operands.permutation(operands.size)
  end

  def operators_permutation(operands)
    permutation_size = operands.size - 1
    OPERATORS.repeated_permutation(permutation_size)
  end

  def with_measurement(renderer)
    starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    yield
    ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    elapsed = ending - starting
    renderer.puts format('Execution time: %.4f secs', elapsed)
  end
end
