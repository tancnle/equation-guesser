# frozen_string_literal: true

require_relative 'equation'

class Calculator
  # Take an arithemtic expression and evaluate
  #
  # @param [Expression]
  # @return [Equation]
  def calculate(expression)
    stack = []

    expression.each do |token|
      if token.is_a?(Numeric)
        stack.push(token)
      else
        second = stack.pop
        first = stack.pop

        case token
        when '+'
          stack.push(first + second)
        when '-'
          stack.push(first - second)
        when '*'
          stack.push(first * second)
        when '/'
          stack.push(first / second)
        end
      end
    end

    Equation.new(expression: expression, result: stack.first)
  rescue ZeroDivisionError
    Equation.new(expression: expression)
  end
end
