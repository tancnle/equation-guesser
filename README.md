```
 _____                  _   _                ____
| ____|__ _ _   _  __ _| |_(_) ___  _ __    / ___|_   _  ___  ___ ___  ___ _ __
|  _| / _` | | | |/ _` | __| |/ _ \| '_ \  | |  _| | | |/ _ \/ __/ __|/ _ \ '__|
| |__| (_| | |_| | (_| | |_| | (_) | | | | | |_| | |_| |  __/\__ \__ \  __/ |
|_____\__, |\__,_|\__,_|\__|_|\___/|_| |_|  \____|\__,_|\___||___/___/\___|_|
         |_|
```

## Assumptions
- Only need consider positive integers (i.e. use integer division).
- Do not consider bracketed sub-expressions (e.g. (1 + 2) * (3 + 4)).
- All numbers are used only once.

## Requirements
- Ruby 3.1.0
- Rake

## Development
Run the default rake task to run all tests. Tests are written using Minitest.

```sh
rake
```

Experiment with the app

```sh
./bin/console

irb> equation_guesser = EquationGuesser.new(Array.new(10) { rand(100) }, 23)
irb> equation_guesser.guess(1000)
```

## Design notes
I was pondering over ways to effectively store/generate permutations of
expressions for given list of operands and operators and come across Reverse
Polish Notation (RPN). Turn out with RPN, we can support bracketed sub-expression
as well ＼(^o^)／

```
Infix notation (1 + 2 * 3 + 4)
RPN notation   (1 4 2 3 * + +)
```

I was keen to explore a more pipeline/functional approach to construct the
algorithm than using imperative loop construct.

1. Generate a guessed expression
1. Evaluate expression
1. Filter out invalid equations (ie. Divisible to zero)
1. Compute difference between equation result and desired result
1. Keep searching until found exact match (ie. result - desired_result = 0)
1. If no exact match found, grab the first solutions corresponding to number of guesses
1. Find the best guess.

After mapping out the above series of transformation, `Enumerator::Lazy` just
popped in my mind and it instantly worked a treat.

I have picked `Minitest` instead of a more familiar choice (i.e. `RSpec`) as a way
to explore different test constructs. Minitest is part of Ruby Standard Library
and hence I can keep this solution free of external dependency and bundle. I felt
bit intimidated at first with instance variables being used through out the tests
instead of lazy evaluated and scoped variable in RSpec. It however serves my
testing objectives in the situation and force me to create small object classes.

I also consciously kept the core part of the application functional and free of
side-effects and leave the console output to the outer `EquationGuesser` class.

## Credits
- [Reverse Polish Notation (RPN)](https://en.wikipedia.org/wiki/Reverse_Polish_notation)
- [Convert from infix notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation#Converting_from_infix_notation)
- [Ruby Enumerator take_until](http://stackoverflow.com/questions/20751856/how-to-stop-iteration-in-a-enumeratorlazy-method)
